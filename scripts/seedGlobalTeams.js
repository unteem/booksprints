#!/usr/bin/env node

const logger = require('@pubsweet/logger')
const { Team } = require('pubsweet-server/src/models')

const makeTeam = async type => {
  const names = {
    productionEditor: 'Production Editor',
    author: 'Author',
  }

  logger.info(`Create ${names[type]} team`)

  const team = new Team({
    global: true,
    members: [],
    name: names[type],
    teamType: type,
  })

  await team.save()
  logger.info(`${names[type]} team successfully created`)
}

const seed = async () => {
  logger.info('### RUNNING GLOBAL TEAMS SEED SCRIPTS ###')
  logger.info('=> Checking if global teams exist...')

  try {
    const teams = await Team.findByField({ global: true })

    const productionEditorTeam = teams.find(
      t => t.teamType === 'productionEditor',
    )
    const authorTeam = teams.find(t => t.teamType === 'author')

    if (productionEditorTeam && authorTeam) {
      logger.info('All global teams found, exiting...')
    } else {
      if (!productionEditorTeam) {
        logger.warn('No Production Editor team found')
        await makeTeam('productionEditor')
      } else {
        logger.info('Production Editor team already exists')
      }

      if (!authorTeam) {
        logger.warn('No Author team found')
        await makeTeam('author')
      } else {
        logger.info('Author team already exists')
      }
    }
  } catch (err) {
    logger.warn('No global teams found')

    await makeTeam('productionEditor')
    await makeTeam('author')
  }

  logger.info('Team seed successfully finished')
}

seed()
